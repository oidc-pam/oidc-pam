# OIDC PAM

A PAM module for authenticating users using OpenID Connect.
This as been tested with keycloak but must work with other OIDC providers (unless they implement the `.well-known/openid-configuration` file).

This project has been built at Télécom Paris and a report has been written about it (in french).
You can find it on [/docs/report_FR.md](/docs/report_FR.md)
